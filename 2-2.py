#!/bin/python3

"""
Quadratic function plotter
"""

import matplotlib.pyplot as plt

def gen_x_values():
    x_values = []
    for i in range(-10, 10, 1):
        x_values.append(i)
    return x_values

def quadratic_values():
    y_values = []
    x_values = gen_x_values()
    for x in x_values:
        y = x**2 + 2*x + 1
        y_values.append(y)
    return y_values

def quadratic_plot():
    plt.plot(gen_x_values(), quadratic_values())
    plt.title("Quadratic values plotted with x value -10 to 10")
    plt.xlabel("X values")
    plt.ylabel("Y Values")
    plt.show()


if __name__ == "__main__":

    quadratic_plot()
    
        
