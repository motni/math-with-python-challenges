#!/bin/python

"""
Fraction operations
"""

from fractions import Fraction

def add(a, b):
    print("Results of addition: {0}".format(a+b))

def minus(a, b):
    print("Results of addition: {0}".format(a-b))

def divide(a, b):
    print("Results of dividsion: {0}".format(a/b))

def multiply(a, b):
    print("Results of multiplication: {0}".format(a*b))


if __name__== '__main__':

  a = Fraction(input("Please enter first fraction in form x/x : "))
  b = Fraction(input("Please enter second fraction in form x/x : "))
  op = input("Operation to perform  +, -, / or * : ")
  
  if op == "+":
      add(a, b)
  if op == "-":
      minus(a, b)
  if op == "/":
      divide(a, b)
  if op == "*":
      multiply(a, b)

