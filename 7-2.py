'''
Use Gradient descent to figure out min value of single variable
function.

'''


from sympy import Derivative, Symbol, sympify, solve
import matplotlib.pyplot as plt

def grad_descent(x0, f1x, x):
    if not solve(f1x):
        print('There is no solution for {0}=0 does not exist.'.format(f1x))
        return None
    epsilon = 1e-16
    step_size=1e-4
    x_old = x0
    x_new = x_old-step_size*f1x.subs({x: x_old}).evalf()

    X_traversed = []
    
    while abs(x_old - x_new) > epsilon:

        X_traversed.append(x_new)
        x_old = x_new
        x_new = x_old - step_size*f1x.subs({x:x_old}).evalf()

        return x_new, X_traversed

def frange(start, final, interval):

    numbers = []
    while start < final:
        numbers.append(start)
        start = start + interval

        return numbers

def create_plot(X_traversed, f, var):
    # graph of the function itself
    x_val = frange(-1, 1, 0.01)
    f_val = [f.subs({var:x}) for x in x_val]
    plt.plot(x_val, 'bo')

    f_traversed = [f.subs({var:x}) for x in X_traversed]
    plt.plot(X_traversed, f_traversed, 'r')
    plt.legend(['Function', 'Intermediate points'], loc='best')
    plt.show()

if __name__ == '__main__':

    f = input('Enter a function in on variable: ')
    var = input('Enter variable in respect this shuold be diffrentiated: ')
    var0 = float(input('Enter the starting value of variable: '))
    try:
        f = sympify(f)
    except SympifyError:
        print('Invalid function entered')
    else:
        var = Symbol(var)
        d = Derivative(f, var).doit()
        var_min, X_traversed = grad_descent(var0, d, var)
        if var_min:
            print('{0}: {1}'.format(var.name, var_min))
            print('Minimum value: {0}'.format(f.subs({var:var_min})))

        create_plot(X_traversed, f, var)





