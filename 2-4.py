#!/bin/python3

"""
Bar char about users expenses
"""

import matplotlib.pyplot as plt

def draw_barchart(labels, cost):
    # Lenght of lables array determines how many bars
    n_bars = len(labels)
    plt.barh(labels, cost, align="center")
    # Settings the identification for bars
    plt.xlabel("Cost")
    plt.ylabel("Label")
    plt.title("My monthly exepenses")
    plt.show()

def n_items():
    n = float(input("How many expenses categories is needed? :"))
    return n

if __name__ == "__main__":

    labels = []
    cost = []
    n_items = n_items()
    i = 0 

    while i < n_items:
        labels.append(input("Please enter a label :"))
        cost.append(float(input("Please enter cost :")))
        i = i +1

    draw_barchart(labels, cost)

