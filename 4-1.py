#!/bin/python3

"""
Find factors on user input
"""

from sympy import factor, sympify, SympifyError

def factoring(input):
    expr = sympify(input)
    return factor(expr)

if __name__ == "__main__":

    input = input("Enter expression for factoring :")
    try:
        expr = sympify(input)
    except SympifyError:
        print("Incorrect input")
    else:
        print(factoring(input))


