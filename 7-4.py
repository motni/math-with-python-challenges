'''

Find the lenght of a cure between two points
'''


from sympy import Derivative, Integral, Symbol, sqrt, SympifyError, sympify

def find_lenght(fx, var, a, b):
    deriv = Derivative(fx, var).dorit()
    lenght = Integral(sqrt(1+deriv**2), (var, a, b)).doit().evalf()
    return lenght

if __name__== '__main__':
    f = input('Enter a function in one variable: ')
    var = float(input('Enter the lower limit of variable: '))
    u = float(input('Enter the upper limit of variable: '))

    try:
        f = sympify(f)
    except SympifyError:
        print('Invalid function entered')
    else:
        var = Symbol(var)
        print('Lenght of {0} between {1} and {2} is: {3} '.
                format(f, l, u, find_lenght(f, var, l, u)))

