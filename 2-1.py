#!/bin/python3

"""
Temperature graph in city of Rovaniemi in January from 1999 to 2019
https://ilmatieteenlaitos.fi/tilastoja-vuodesta-1961
"""

import matplotlib.pyplot as plt

def create_graph():

    average_temps = [-9.7, -14.3, -11.5, -10.8, -9.9, -12.2, -10.3, -15.3, -8.3, -9.9 ,-13.9]
    change_from_average = [1.7, -2.9, -0.1, 0.6, 1.5, -0.8, 1.1, -3.9, 3.1, 1.5, -2.5]
    
    plt.plot(range(2009, 2020, 1), average_temps, marker='x')
    plt.plot(range(2009, 2020, 1), change_from_average, marker='o')
    plt.xlabel("Years")
    plt.ylabel("Temperatures")
    plt.title("Average temperatures and diffrenciation from average")
    plt.legend(["Diffrenciation from average", "Average temp"])
    plt.show()


if __name__ == "__main__":

    create_graph()
