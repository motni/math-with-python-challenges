'''
How many coin tosses until player runs out of money
'''

import random

def toss(money):

    win = 1
    loss = 1.5

    current_money = money
    tosses = 0

    while current_money > 0:
        tosses += 1
        toss = random.randint(0, 1)
        
        if toss == 0:
            current_money += win
            print('Tails! current money is: {0}'.format(current_money))
        else: 
            current_money -= loss
            print('Heads! current money is: {0}'.format(current_money, tosses))

    print('Game over! current money is: {0}. Coin was tossed: {1}'.format(current_money, tosses))

if __name__ == '__main__':
    
    money = float(input('How much money do you have: '))
    toss(money)


        
