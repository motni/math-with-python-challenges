#!/bin/python3

"""
Read numbers from mydate.txt and print out mean, meadian,
mode, variance and standard deviation.
"""

from collections import Counter


def import_file(filename):
    numbers = []
    with open(filename) as f:
            for line in f:
                numbers.append(float(line))
    return numbers

def calc_mean(numbers):
    N = len(numbers)
    mean = sum(numbers)/N
    return mean

def calc_median(numbers):
    N = len(numbers)
    numbers.sort()

    if N % 2 == 0:
        m1 = N/2
        m2 = (N/2) + 1

        m1 = int(m1) - 1
        m2 = int(m2) - 2

        median = (numbers[m1] + numbers[m2]) / 2
    else:
        m = (N+1)/2
        m = int(m) - 1
        median = numbers[m]

    return median

def calc_mode(numbers):
    c = Counter(numbers)
    mode = c.most_common(1)
    return mode[0][0]

def calc_diff(numbers):
    mean = calc_mean(numbers)
    diff = []

    for num in numbers:
        diff.append(num-mean)
    return diff

def calc_variance(numbers):
    diff = calc_diff(numbers)
    squared_diff = []

    for d in diff:
        squared_diff.append(d**2)
    sum_squared_diff = sum(squared_diff)
    variance = sum_squared_diff/len(numbers)
    return variance

if __name__ == "__main__":

    numbers = import_file("mydata.txt")
    print("Mean is : {0}".format(calc_mean(numbers)))
    print("Median is: {0}".format(calc_median(numbers)))
    print("mode is: {0}".format(calc_mode(numbers)))
    print("Diffrence is : {0}".format(calc_diff(numbers)))
    print("Variance is : {0}".format(calc_variance(numbers)))
    std = calc_variance(numbers)**0.5
    print("Stanard Deviation is : {0}".format(std))
