#!/bin/python3

'''
Unit converter: Miles and Kilometers
'''

def print_menu():
    print('1. Kilometers to Miles')
    print('2. Miles to Kilometers')
    print('3. Pounds to Kilos')
    print('4. Kilos to pounds')
    print('5. Fahrenheit to Celsius')
    print('6. Celsius to Fahrenheit')

def km_miles():
    km = float(input("Enter distance in kilometers: "))
    miles = km / 1.609

    print("Distance in miles: {0}".format(miles))

def miles_km():
    miles = float(input("Enter distance in miles: "))
    km = miles * 1.609

    print("Distance in kilometers: {0}".format(km))

def pounds_kilos():
    pounds = float(input("Enter weight in pounds"))
    kilos = pounds * 2.2046

    print("Weight in kilos is :{0}".format(kilos))

def kilos_pounds():
    kilos = float(input("Enter weight in kilos"))
    pounds = kilos / 2.2046 

    print("Weight in pounds is : {0}".format(pounds))

def fahrenheit_celsius():
    fahrenheit = float(input("Enter temp in Fahrenheit : "))
    celsius = (fahrenheit - 32) / 1.8000

    print("Temp in celsius is: {0}".format(celsius))

def celsius_fahrenheit():
    celsius = float(input("Enter temp in Celsius"))
    fahrenheit = (celsius * 1.800) + 32

    print("Temp in Fahrenheit is: {0}".format(fahrenheit))

    
if __name__ == "__main__":

    print_menu()
    choice = input("Which conversion would you like to do?: ")
    if choice == "1":
        km_miles()

    if choice == "2":
        miles_km()

    if choice == "3":
        pounds_kilos()

    if choice == "4":
        kilos_pounds()

    if choice == "5":
        fahrenheit_celsius()

    if choice == "6":
        celsius_fahrenheit()
