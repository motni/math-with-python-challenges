#!/bin/python

"""
Even or odd number printing
"""

def evenodd(a):
    if a % 2 == 0:
        return "even"
    else:
        return "odd"

def morenumbers(a):
    for i in range(0, 9):
        a = a + 2
        yield a

if  __name__ == '__main__':

    a = input("Please enter a number: ")
    a = float(a)

    if a.is_integer():
        a = int(a)
        print(evenodd(a))
        for i in morenumbers(a):
            print(i)
    else:
        print("Please enter int")
    
