#!/bin/python3

"""
Fibonacci numbers ratio plotted
"""

from matplotlib import pyplot as plt

# Generate set of fibonacci numbers
def fibo(n):
    if n == 1:
        return[1]
    if n == 2:
        return [1, 1]
    # Longer series
    a = 1
    b = 1
    series = [a, b]
    for i in range(n):
        c = a + b
        series.append(c)
        a = b
        b = c
    return series

# Plot the numbers
def plotfibo(fibos):
    ratios = []
    for i in range(len(fibos)-1):
        ratios.append(fibos[i+1]/fibos[i])
    plt.plot( ratios)
    plt.title("The ratios in fibonacci numbers")
    plt.xlabel("Nth number")
    plt.ylabel("Ratio") 
    plt.show()

if __name__ == "__main__":

    plotfibo(fibo(100))
