#!/bin/python3

"""
Percentile calculator from file of numbers
"""

import sys

def calc_percentile(numbers, percentile):

    # not trusting user input
    if percentile < 0 or percentile > 100:
        return None

    numbers.sort()
    # return obvious results
    if percentile == 0:
        return numbers[0]
    if percentile == 100: 
        return numbers[-1]
    
    
    n = len(numbers)
    rank = (percentile/100)*(n-1) + 1

    k = int(rank)
    d = rank - k

    real_idx_1 = k-1
    real_idx_2 = k

    return numbers[real_idx_1] + d*(numbers[real_idx_2]-numbers[real_idx_1])

def data_in(filename):
    numbers = []
    with open(filename) as f:
        for line in f:
            numbers.append(float(line))
    return numbers

if __name__ == "__main__":

    filename = sys.argv[1]
    numbers = data_in(filename)

    percentile = int(input("Please enter a percentile number you want :"))
    percentile_score = calc_percentile(numbers, percentile)

    if percentile_score:
        print("The score at {0} percentile: {1}".format(percentile, percentile_score))
    else:
        print("Could not find the score corresponding to {0} percentile".format(percentile))


