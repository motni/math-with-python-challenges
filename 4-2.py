#!/bin/python3

"""
Graphical equation factoring
"""

from sympy import Symbol, sympify, SympifyError, solve
from sympy.plotting import plot as plt


def solver(expr1, expr2, x, y):

    sol = solve((expr1, expr2), dict=True)

    if sol:
        print('x is: {0} and y is: {1}'.format(sol[0][x], sol[0][y]))
    else:
        print('Unable to find solution')

def plotter(expr1, expr2):
    sol1_y = solve(expr1, 'y')[0]
    sol2_y = solve(expr2, 'y')[0]
    plt(sol1_y, sol2_y, legend=True)



if __name__ == '__main__':

   expr1 = input('Please enter your first expression in terms of X and y :')
   expr2 = input('Please enter your second expression in terms of X and y :')

   try:
        expr1 = sympify(expr1)
        expr2 = sympify(expr2)
   except SympifyError:
        print('Not valid expression')
   else:
        x = Symbol('x')
        y = Symbol('y')

        expr1_symbols = expr1.atoms(Symbol)
        expr2_symbols = expr2.atoms(Symbol)

        if len(expr1_symbols) > 2 or len(expr2_symbols) > 2:
            print('The expressions must have only two variables like x and y ')
        elif x not in expr1_symbols or y not in expr1_symbols:
            print('The first equation should have x and y')
        elif x not in expr2_symbols or y not in expr2_symbols: 
            print('The second equation should have x and y')
        else:
            solver(expr1, expr2, x, y)
            plotter(expr1, expr2)

