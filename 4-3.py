#!/bin/python3

"""
Summing arbitrary series
"""

from sympy import summation, sympify, Symbol, pprint

def series_sum(nth_term, max_terms):
    n = Symbol("n")
    s = summation(nth_term, (n, 1, max_terms))
    pprint(s)


if __name__ == '__main__':
    nth_term = sympify(input("Please etner the nth term: "))
    max_terms = int(input("Please enter how many terms there should be: "))

    series_sum(nth_term, max_terms)
