'''
Check if funtion continues

'''


from sympy import Limit, Symbol, sympify, SympifyError

def verify_continuity(f, var, a):
    l1 = Limit(f, var, a, dir='+').doit()
    l2 = Limit(f, var, a, dir='-').doit()
    f_val = f.subs({var:a})

    if l1 == l2 and f_val == l1:
        print('{0} is continnuous at {1}'.format(f, a))
    else:
        print('{0} is not continnuous at {1}'.format(f, a))

if __name__ =='__main__':

    f = input('Enter a function in one variable: ')
    var = input('Enter the variable: ')
    a = float(input('Enter the point to check the contiuity at : '))
    try: 
        f = sympify(f)
    except SympifyError:
        print('Invalid function entered')
    else:
        var = Symbol(var)
        d = verify_continuity(f, var, a)

