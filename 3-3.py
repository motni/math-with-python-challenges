#!/bin/python3

"""
simple stats and plot from csv file
"""

from collections import Counter
import csv, sys
import matplotlib.pyplot as plt


def import_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        next(reader)

        column1 = []
        column2 = []

        for row in reader:
            column1.append(float(row[0]))
            column2.append(float(row[1]))

    return column1, column2

def calc_mean(numbers):
    N = len(numbers)
    mean = sum(numbers)/N
    return mean

def calc_median(numbers):
    N = len(numbers)
    numbers.sort()

    if N % 2 == 0:
        m1 = N/2
        m2 = (N/2) + 1

        m1 = int(m1) - 1
        m2 = int(m2) - 2

        median = (numbers[m1] + numbers[m2]) / 2
    else:
        m = (N+1)/2
        m = int(m) - 1
        median = numbers[m]

    return median

def calc_mode(numbers):
    c = Counter(numbers)
    mode = c.most_common(1)

    return mode[0][0]

def calc_diff(numbers):
    mean = calc_mean(numbers)
    diff = []

    for num in numbers:
        diff.append(num-mean)

    return diff

def calc_variance(numbers):
    diff = calc_diff(numbers)
    squared_diff = []

    for d in diff:
        squared_diff.append(d**2)
    sum_squared_diff = sum(squared_diff)
    variance = sum_squared_diff/len(numbers)

    return variance

def calc_plot(numbers1, numbers2):
    plt.plot(numbers1, numbers2)
    plt.show()

if __name__ == "__main__":

    filename = sys.argv[1]
    numbers1, numbers2  = import_csv(filename)
    print("Mean is : {0}".format(calc_mean(numbers2)))
    print("Median is: {0}".format(calc_median(numbers2)))
    print("mode is: {0}".format(calc_mode(numbers2)))
    print("Variance is : {0}".format(calc_variance(numbers2)))
    std = calc_variance(numbers2)**0.5
    print("Stanard Deviation is : {0}".format(std))
    calc_plot(numbers1, numbers2)
