'''
Drawing a Venn diagram for two sets from csv file
'''

from matplotlib_venn import venn2
import matplotlib.pyplot as plt
from sympy import FiniteSet
import csv as csv

def read_csv(filename):
    football = []
    others = []
    with open(filename) as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            if row[1] == '1':
                football.append(row[0])
                if row[2] == '1':
                    others.append(row[0])
                    print(football)
                    print(others)
                    return football, others

def draw_venn(data1, data2):

    venn2(subsets=(data1, data2))
    plt.show()


if __name__ == '__main__':

    # read data in 
    data1, data2 = read_csv('sports.csv')
    # data converted to sets which I did find out from solution
    d1 = FiniteSet(*data1)
    d2 = FiniteSet(*data2)

    draw_venn(d1, d2)

