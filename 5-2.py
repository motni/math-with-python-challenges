'''
Challenge to roll dice n amount to see how it always get's close to 3.5
Modified this one to see how many rolls it would need to get to 3.5
'''

import random

def rolls(times):
    rolls = []
    for t in range(times):
        rolls.append(random.randint(1, 6))
    return sum(rolls)/times


if __name__ == '__main__':

    expected_value = 3.5
    print('The expected value should be: {0}'.format(expected_value))
    average = 0
    trial = 0
    while average != 3.5:
        trial = trial + 1
        average = rolls(trial)
        print('Trials: {0} Trial average {1}'.format(trial, average))
